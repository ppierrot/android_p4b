package com.example.projet;

public class Contact {
	private int id;
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	private String address;

	public Contact(int id, String firstname, String lastname) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
	}
}
