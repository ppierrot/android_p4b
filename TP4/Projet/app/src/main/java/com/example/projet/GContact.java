package com.example.projet;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class GContact{

	private ArrayList<Contact> contactList = new ArrayList<Contact>();

	public void loadContacts(JSONArray contacts) {

        for (int i = 0; i < contacts.length(); i++)
        {
            try {
                JSONObject j = contacts.getJSONObject(i);
                contactList.add(new Contact(j.getInt("id"), j.getString("firstname"), j.getString("lastname")));
                }
            catch(Exception e) {

            }

        }
        return contactList;
	}
}

