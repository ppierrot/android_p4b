Projet P4b : client mobile pour "AddressBook"
=============================================

L'objectif de ce projet est de réaliser un client mobile pour l'API AddressBook développée dans le module W4a.

Fonctionnalités
---------------

L'application doit comporter un certain nombre de fonctionnalités :

- **Afficher la liste des contacts** : lorsqu'un contact est sélectionné, les informations de ce contact doivent s'afficher, ainsi que les groupes auxquels il appartient.
- **Afficher la liste des groupes** : lorsqu'un groupe est sélectionné, la liste des contacts appartenant à ce groupe doit s'afficher.
- **Créer/supprimer un contact**
- **Créer/supprimer un groupe**
- **Ajouter/retirer un contact à un groupe**

Le client mobile peut aussi permettre d'**associer une photo à un contact**, mais uniquement localement : la photo est stockée sur le téléphone, pas sur le serveur.

Vous pourrez tester des parties de votre client mobile avec l'API OpenWeatherMap, en attendant que votre API soit fonctionnelle.

Architecture proposée
---------------------

1. L'application est composée d'une activité principale. Cette activité contiendra un [`NavigationDrawer`](https://developer.android.com/training/implementing-navigation/nav-drawer#java) qui permettra de naviguer entre les vues. Chaque vue sera affichée dans un `Fragment`.
2. L'écran d'accueil de l'application affiche la vue de la liste des contacts.
3. Les listes d'éléments (contacts et groupes) sont affichés dans des `RecyclerView`. 
4. Quand un contact est sélectionné, les informations du contact sont affichées dans un nouveau fragment.
5. Quand un groupe est sélectionné, les contacts inscrits dans ce groupe sont affichés dans un nouveau fragment.


Planning proposé
----------------

### 26 février : Interface graphique générale et RecyclerView

1. Mettre en place les principaux éléments de l'application (activité principale, NavigationDrawer, fragments).
2. Mettre en place le [`RecyclerView`](https://developer.android.com/guide/topics/ui/layout/recyclerview) des contacts :

    a. Écrire une classe `ContactViewHolder` qui étend `RecyclerView.ViewHolder` et chargée d'afficher un item du `RecyclerView`. Le layout associé doit être composé de deux `TextView` pour afficher le nom et le prénom du contact.

    b. Écrire une classe `ContactAdapter` qui étend `RecyclerView.Adapter <ContactViewHolder>`. Un attribut de cette classe sera chargé de stocker les informations des contacts à afficher dans le RecyclerView.

### 5 mars : Récupération des données du serveur

On visera la version 8 d'Android (dans le fichier `build.gradle` : `targetSdkVersion=26`). En effet la version 9 interdit par défaut les connexions *cleartext* (protocole `http`) et pour les autoriser il faut rajouter un fichier XML dédié. Voir : [https://android-developers.googleblog.com/2018/04/protecting-users-with-tls-by-default-in.html](https://android-developers.googleblog.com/2018/04/protecting-users-with-tls-by-default-in.html).

1. Écrire les requêtes permettant de récupérer la liste des contacts sur le serveur
2. Afficher la liste dans la vue des contacts
2. De la même manière récupérer et afficher la liste des groupes
3. Implémenter la vue permettant d'éditer un contact existant et d'ajouter un nouveau contact
4. Écrire la requête permettant d'ajouter/modifier ce contact sur le serveur

### 12 et 19 mars 

L'objectif est d'implémenter et de tester l'ensemble des fonctionnalités.

**Vue des contacts**  
- la liste des contacts disponibles sur le serveur est affichée. 
- lorsqu'un contact est sélectionné, la vue d'édition du contact est affichée. Une fois le contact modifié les informations sont mises à jour sur le serveur.
- un contact peut être associé à un ou plusieurs groupes.
- un contact peut être supprimé
- Pour la gestion des clics sur les items du `RecyclerView` consulter :   [http://sapandiwakar.in/recycler-view-item-click-handler](http://sapandiwakar.in/recycler-view-item-click-handler)
- Un élément graphique ou un élément du menu doit permettre la création d'un nouveau contact. Une fois le contact édité, les informations sont envoyées au serveur.

**Vue des groupes**

- la liste des groupes disponibles sur le serveur est affichée.
Lorsqu'un groupe est sélectionné, la liste des contacts appartenant à ce groupe s'affiche.
- un élément graphique ou un élément du menu doit permettre la création d'un nouveau groupe.
- un groupe peut être supprimé

**Photo**
- le client mobile peut aussi permettre d'**associer une photo à un contact**, mais uniquement localement : la photo est stockée sur le téléphone, pas sur le serveur.
