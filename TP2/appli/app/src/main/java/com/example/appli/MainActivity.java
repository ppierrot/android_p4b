package com.example.appli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MainActivity extends AppCompatActivity {

    class TestFragment extends Fragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { //la vue du fragment est décrite dans le fichier XML `res/layout/fragment.xml`
            super.onCreateView(inflater, container, savedInstanceState);//"intégrée" (inflated) dans le fragment
            return inflater.inflate(R.layout.fragment,container,false);
        }

        @Override
        public void onPause() {
            // TODO Auto-generated method stub
            super.onPause();
        }
    }
}
