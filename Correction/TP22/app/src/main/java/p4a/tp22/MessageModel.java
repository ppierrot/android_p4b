package p4a.tp22;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class MessageModel extends ViewModel {

    public final MutableLiveData<String> _message = new MutableLiveData<String>();

    public void setMessage(String text) {
        _message.setValue(text);
    }

    public LiveData<String> getMessage() {
        return _message;
    }

}
