package p4a.tp22;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOne extends Fragment {

    private MessageModel _messageModel;

    public FragmentOne() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _messageModel = ViewModelProviders.of(getActivity()).get(MessageModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        return inflater.inflate(R.layout.fragment_fragment_one, container, false);
    }

    @Override
    public void onStop() {
        super.onStop();
        EditText messageToSend = getView().findViewById(R.id.inputMessage);
        _messageModel.setMessage(messageToSend.getText().toString());
    }
}
