package com.example.projet;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.projet.controller.GContact;
import com.example.projet.model.Contact;
import com.example.projet.model.MailAddress;
import com.example.projet.model.Phone;
import com.example.projet.model.PostalAddress;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragNewContact.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragNewContact#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragNewContact extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragNewContact() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragNewContact.
     */
    // TODO: Rename and change types and number of parameters
    public static FragNewContact newInstance(String param1, String param2) {
        FragNewContact fragment = new FragNewContact();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // Créer un nv contact (en local)
    public void onClickNew(){
        String firstname = ((EditText) getView().findViewById(R.id.txtFirstname)).getText().toString();
        String lastname = ((EditText) getView().findViewById(R.id.txtLastname)).getText().toString();
        String address = ((EditText) getView().findViewById(R.id.txtAddress)).getText().toString();
        String phone = ((EditText) getView().findViewById(R.id.txtPhone)).getText().toString();
        String mail = ((EditText) getView().findViewById(R.id.txtMail)).getText().toString();


        try {
            int rbChecked;
            RadioGroup rgAddress = ((RadioGroup) getView().findViewById(R.id.rgAddress));
            rbChecked = rgAddress.getCheckedRadioButtonId();
            String typeAddress = ((RadioButton) getView().findViewById(rbChecked)).getText().toString();

            int rbCheckedMail;
            RadioGroup rgMail = ((RadioGroup) getView().findViewById(R.id.rgMail));
            rbCheckedMail = rgAddress.getCheckedRadioButtonId();
            String typeMail = ((RadioButton) getView().findViewById(rbCheckedMail)).getText().toString();
            int rbCheckedPhone;
            RadioGroup rgPhone = ((RadioGroup) getView().findViewById(R.id.rgPhone));
            rbCheckedPhone = rgAddress.getCheckedRadioButtonId();
            String typePhone = ((RadioButton) getView().findViewById(rbCheckedPhone)).getText().toString();

            ArrayList<Phone> phones = new ArrayList<Phone>();
            phones.add(new Phone(phone, typePhone));
            ArrayList<PostalAddress> addresses = new ArrayList<PostalAddress>();
            addresses.add(new PostalAddress(address, typeAddress));
            ArrayList<MailAddress> mails = new ArrayList<MailAddress>();
            mails.add(new MailAddress(mail, typeMail));
            GContact.getContactList().add(new Contact(0, firstname, lastname, phones, addresses, mails));

            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            FragContacts fragment1 = new FragContacts();
            transaction.replace(R.id.layoutFrag, fragment1);
            transaction.commit();
        }

        catch (Exception e) {
            TextView txtError = (TextView) getView().findViewById(R.id.textError);
            txtError.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_frag_new_contact, container, false);
        Button btnNew = (Button) myView.findViewById(R.id.btnNewContact);
        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNew();
            }
        });
        return myView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
