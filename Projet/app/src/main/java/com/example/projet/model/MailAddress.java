package com.example.projet.model;

public class MailAddress {
    String mail;
    String type;

    public MailAddress(String mailAddress, String type){
        this.mail = mailAddress;
        this.type = type;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
