package com.example.projet.request;

import android.util.Log;

import com.example.projet.controller.GGroup;

import org.json.JSONArray;

import java.net.MalformedURLException;
import java.net.URL;

public class GroupRequest implements ApiRequestGet.OnTaskCompleted
{
    /**
     * Requête pour charger les groupes
     */

    public void loadGroups(){
        //Some url endpoint that you may have
        String url = "http://130.79.81.65:3000/group";
        //Instantiate new instance of our class
        ApiRequestGet getRequest = new ApiRequestGet( this);
        try {
            URL myURL = new URL(url);
            getRequest.execute(myURL);
        }
        catch (MalformedURLException e){
            Log.d("Erreur", "Impossible d'accéder aux données, " + e.toString());
        }
    }

    public void onTaskCompleted(JSONArray data){
        GGroup.loadGroups(data);
    }

}