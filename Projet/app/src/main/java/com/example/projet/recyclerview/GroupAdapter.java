package com.example.projet.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.example.projet.R;
import com.example.projet.model.Group;

import java.util.ArrayList;

public class GroupAdapter  extends RecyclerView.Adapter<GroupViewHolder> {
    private ArrayList<Group> groups;

    public GroupAdapter(ArrayList<Group> groups) {
        this.groups = groups;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {
        LinearLayout l = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_view, parent, false);

        GroupViewHolder vh = new GroupViewHolder(l);
        return vh;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        Group group = groups.get(position);
        holder.affichage(group);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return groups.size();
    }
}
