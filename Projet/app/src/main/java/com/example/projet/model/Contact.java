package com.example.projet.model;

import java.lang.reflect.GenericArrayType;
import java.util.ArrayList;

public class Contact {
	private int id;
	private String firstname;
	private String lastname;
	private ArrayList<MailAddress> email;
	private ArrayList<Phone> phone;
	private ArrayList<PostalAddress> address;

	public Contact(int id, String firstname, String lastname, ArrayList<Phone> phones, ArrayList<PostalAddress> addresses, ArrayList<MailAddress> mails ) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.phone = phones;
		this.address = addresses;
		this.email = mails;
	}

	public int getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public ArrayList<MailAddress> getEmail() {
		return email;
	}

	public ArrayList<Phone> getPhone() {
		return phone;
	}

	public ArrayList<PostalAddress> getAddress() {
		return address;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setEmail(ArrayList<MailAddress> email) {
		this.email = email;
	}

	public void setPhone(ArrayList<Phone> phone) {
		this.phone = phone;
	}

	public void setAddress(ArrayList<PostalAddress> address) {
		this.address = address;
	}
}
