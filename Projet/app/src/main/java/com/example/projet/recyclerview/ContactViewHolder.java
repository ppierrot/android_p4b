package com.example.projet.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.projet.R;
import com.example.projet.model.Contact;

public class ContactViewHolder extends RecyclerView.ViewHolder {

	public TextView nom;
	public TextView prenom;
	public View layout;

	public ContactViewHolder(View v){
		super(v);
		layout = v;
		this.nom = (TextView) v.findViewById(R.id.txtNom);
		this.prenom = (TextView) v.findViewById(R.id.txtPrenom);;
	}

	public void affichage(Contact contact) {
		nom.setText(contact.getLastname());
		prenom.setText(contact.getFirstname());
	}
}
