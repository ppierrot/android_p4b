package com.example.projet.request;

import android.os.AsyncTask;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiRequestGroup extends AsyncTask<URL, Void, JSONArray> {

    ApiRequestGroup.OnGroupsLoaded myListener;


    public ApiRequestGroup(ApiRequestGroup.OnGroupsLoaded listener){
        myListener = listener;
    }

    @Override
    protected JSONArray doInBackground(URL... params){


        URL url = params[0];
        JSONArray result;
        String inputLine;

        try {
            //Create a connection
            HttpURLConnection connection =(HttpURLConnection) url.openConnection();

            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            result = new JSONArray(stringBuilder.toString());
        }

        catch(Exception e){
            e.printStackTrace();
            result = null;
        }

        return result;

    }

    @Override
    protected void onPostExecute(JSONArray result){
        myListener.onGroupsLoaded(result);
    }

    public interface OnGroupsLoaded{
        public void onGroupsLoaded(JSONArray result);
    }

}
