package com.example.projet.model;

public class Group{
	private int id;
	private String title;

	public Group(int id, String title){
		this.title = title;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
}
