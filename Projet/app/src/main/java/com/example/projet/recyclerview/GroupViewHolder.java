package com.example.projet.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.projet.R;
import com.example.projet.model.Group;

public class GroupViewHolder  extends RecyclerView.ViewHolder  {

    public TextView nom;
    public View layout;

    public GroupViewHolder(View v){
        super(v);
        layout = v;
        this.nom = (TextView) v.findViewById(R.id.txtName);
    }

    public void affichage(Group group) {
        nom.setText(group.getTitle());
    }
}
