package com.example.projet.controller;

import android.provider.Telephony;
import android.util.Log;

import com.example.projet.model.Contact;
import com.example.projet.model.Group;
import com.example.projet.model.MailAddress;
import com.example.projet.model.Phone;
import com.example.projet.model.PostalAddress;
import com.example.projet.request.AddressRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GContact{

	private static ArrayList<Contact> contactList = new ArrayList<Contact>();
	private static ArrayList<Group> contactGroup = new ArrayList<>();

	private static int idTransmitted = 0;

	public static ArrayList<Group> loadGroups(JSONArray groups) {
	    contactGroup.clear();
		try {
			for (int i = 0; i < groups.length(); i++) {
				JSONObject jsonObj = groups.getJSONObject(i);

				int id = jsonObj.getInt("id");
				String title = jsonObj.getString("title");

				contactGroup.add(new Group(id, title));
			}
		}

		catch (Exception e) {
			Log.d("Erreur", "Il y a une erreur au chargement des groupes");
		}
		return contactGroup;
	}

	public static ArrayList<Contact> loadContacts(JSONArray contacts) {

	//	Log.d("Test", contacts.toString());
		try {
			for (int i = 0; i < contacts.length(); i++) {
				JSONObject jsonObj = contacts.getJSONObject(i);

				int id = jsonObj.getInt("id");
				String firstname = jsonObj.getString("firstname");
				String lastname = jsonObj.getString("lastname");
				ArrayList<Phone> phones = new ArrayList<>() ;
				ArrayList<MailAddress> mails = new ArrayList<>() ;
				ArrayList<PostalAddress> addresses = new ArrayList<>();

				contactList.add(new Contact( id, firstname , lastname, phones, addresses, mails ));
			}
		}

		catch (Exception e) {
				Log.d("Erreur", "Il y a une erreur au chargement du contact");
		}
		return contactList;
	}

	public static int getIdTransmitted() {
		return idTransmitted;
	}

	public static void setIdTransmitted(int idTransmitted) {
		GContact.idTransmitted = idTransmitted;
	}

	public static ArrayList<Contact> getContactList() {
		return contactList;
	}

    public static ArrayList<Group> getContactGroup() {
        return contactGroup;
    }
}
