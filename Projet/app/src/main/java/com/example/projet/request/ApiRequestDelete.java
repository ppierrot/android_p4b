package com.example.projet.request;

import android.os.AsyncTask;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiRequestDelete  extends AsyncTask<URL, Void, JSONArray> {
    OnDeleteCompleted myListener;


    public ApiRequestDelete(OnDeleteCompleted listener){
        myListener = listener;
    }

    @Override
    protected JSONArray doInBackground(URL... params){


        URL url = params[0];
        JSONArray result;
        String inputLine;

        try {
            //Create a connection
            HttpURLConnection connection =(HttpURLConnection) url.openConnection();
            connection.setRequestMethod("DELETE");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            result = new JSONArray(stringBuilder.toString());
        }

        catch(Exception e){
            e.printStackTrace();
            result = null;
        }

        return result;

    }

    @Override
    protected void onPostExecute(JSONArray result){
        myListener.onDeleteCompleted(result);
    }

    public interface OnDeleteCompleted{
        public void onDeleteCompleted(JSONArray result);
    }
}
