package com.example.projet.request;

import android.util.Log;

import com.example.projet.controller.GContact;

import org.json.JSONArray;

import java.net.MalformedURLException;
import java.net.URL;

public class ContactRequest implements ApiRequestGet.OnTaskCompleted, ApiRequestDelete.OnDeleteCompleted, ApiRequestGroup.OnGroupsLoaded
{
    /**
     * Requête pour charger les contacts
     */

    public void loadContacts(){
        //Some url endpoint that you may have
        String url = "http://130.79.81.65:3000/person";
        //Instantiate new instance of our class
        ApiRequestGet getRequest = new ApiRequestGet( this);
        try {
            URL myURL = new URL(url);
            getRequest.execute(myURL);
        }
        catch (MalformedURLException e){
            Log.d("Erreur", "Impossible d'accéder aux données, " + e.toString());
        }
    }

    public void onTaskCompleted(JSONArray data){
        GContact.loadContacts(data);
    }

    // Supprimer le contact
    public void deleteContact(int id){
        //Some url endpoint that you may have
        String url = "http://130.79.81.65:3000/person/" + id;
        //Instantiate new instance of our class
        ApiRequestDelete delRequest = new ApiRequestDelete( this);
        try {
            URL myURL = new URL(url);
            delRequest.execute(myURL);
        }
        catch (MalformedURLException e){
            Log.d("Erreur", "Impossible d'accéder aux données, " + e.toString());
        }
    }

    @Override
    public void onDeleteCompleted(JSONArray result) {

    }

    /**
     * Requête pour charger les groupes du contact
     */

    public void loadGroups(int id) {
        //Some url endpoint that you may have
        String url = "http://130.79.81.65:3000/person/" + id + "/group";
        //Instantiate new instance of our class
        ApiRequestGroup getRequest = new ApiRequestGroup( this);
        try {
            URL myURL = new URL(url);
            getRequest.execute(myURL);
        }
        catch (MalformedURLException e){
            Log.d("Erreur", "Impossible d'accéder aux données, " + e.toString());
        }
    }

    public void onGroupsLoaded(JSONArray data){
        GContact.loadGroups(data);
    }

}
