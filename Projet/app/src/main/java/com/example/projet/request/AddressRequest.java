package com.example.projet.request;

import android.util.Log;

import com.example.projet.controller.GContact;

import org.json.JSONArray;

import java.net.MalformedURLException;
import java.net.URL;

public class AddressRequest implements ApiRequestGet.OnTaskCompleted {

    // Recupere les adresses du contact
    public void requestPostalAddress(final int index) {
        String url = "http://130.79.81.65:3000/person/" + GContact.getContactList().get(index).getId() + "/postaladdress";
        ApiRequestGet getRequest = new ApiRequestGet( this);
        try {
            URL myURL = new URL(url);
            getRequest.execute(myURL);
        }
        catch (MalformedURLException e){
            Log.d("Erreur", "Impossible d'accéder aux données, " + e.toString());
        }
    }

    public void onTaskCompleted(JSONArray data){
        GContact.loadContacts(data);
    }


}
