package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.projet.controller.GContact;
import com.example.projet.model.Contact;
import com.example.projet.recyclerview.ContactAdapter;
import com.example.projet.request.ContactRequest;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragContacts.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragContacts#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragContacts extends Fragment implements FragNewContact.OnFragmentInteractionListener, FragInfoContact.OnFragmentInteractionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView recyclerViewContacts;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<Contact> myContacts = GContact.getContactList();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragContacts() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragContacts.
     */
    // TODO: Rename and change types and number of parameters
    public static FragContacts newInstance(String param1, String param2) {
        FragContacts fragment = new FragContacts();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_frag_contacts, container, false);

        recyclerViewContacts = (RecyclerView) rootView.findViewById(R.id.recyclerViewContacts);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewContacts.setLayoutManager(layoutManager);

        mAdapter = new ContactAdapter(myContacts);
        recyclerViewContacts.setAdapter(mAdapter);

        Button btnAdd = (Button)rootView.findViewById(R.id.btnAddContacts);

        // Swipe gauche
        ItemTouchHelper.SimpleCallback itemTouchHelperCallbackLeft = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerViewContacts, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                ContactRequest reqContact = new ContactRequest();
                Contact contact = myContacts.get(viewHolder.getAdapterPosition());
                reqContact.deleteContact(contact.getId());

                GContact.getContactList().remove(viewHolder.getAdapterPosition());

                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                FragContacts fragment1 = new FragContacts();
                transaction.replace(R.id.layoutFrag, fragment1);
                transaction.commit();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerViewContacts, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                // view the background view
            }
        };
        // Swipe droit
        ItemTouchHelper.SimpleCallback itemTouchHelperCallbackRight = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerViewContacts, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                GContact.setIdTransmitted(viewHolder.getAdapterPosition());
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                FragInfoContact fragment1 = new FragInfoContact();
                transaction.replace(R.id.layoutFrag, fragment1);
                transaction.commit();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerViewContacts, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                // view the background view
            }
        };

        new ItemTouchHelper(itemTouchHelperCallbackLeft).attachToRecyclerView(recyclerViewContacts);
        new ItemTouchHelper(itemTouchHelperCallbackRight).attachToRecyclerView(recyclerViewContacts);

        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickBtnAdd();
            }
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    // Clic sur le bouton add, affichage du frag de new contact
    public void onClickBtnAdd(){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        FragNewContact fragment1 = new FragNewContact();
        transaction.replace(R.id.layoutFrag, fragment1);
        transaction.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {    }


}
