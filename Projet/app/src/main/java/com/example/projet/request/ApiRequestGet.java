package com.example.projet.request;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiRequestGet extends AsyncTask<URL, Void, JSONArray> {

    OnTaskCompleted myListener;


    public ApiRequestGet(OnTaskCompleted listener){
        myListener = listener;
    }

    @Override
    protected JSONArray doInBackground(URL... params){


        URL url = params[0];
        JSONArray result;
        String inputLine;

        try {
            //Create a connection
            HttpURLConnection connection =(HttpURLConnection) url.openConnection();

            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            result = new JSONArray(stringBuilder.toString());
        }

        catch(Exception e){
            e.printStackTrace();
            result = null;
        }

        return result;

    }

    @Override
    protected void onPostExecute(JSONArray result){
        myListener.onTaskCompleted(result);
    }

    public interface OnTaskCompleted{
        public void onTaskCompleted(JSONArray result);
    }

}
