package com.example.projet.controller;

import android.util.Log;

import com.example.projet.model.Contact;
import com.example.projet.model.Group;
import com.example.projet.model.MailAddress;
import com.example.projet.model.Phone;
import com.example.projet.model.PostalAddress;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GGroup{
	private static ArrayList<Group> groupList = new ArrayList<Group>();


	public static ArrayList<Group> loadGroups(JSONArray groups) {

		//	Log.d("Test", contacts.toString());
		try {
			for (int i = 0; i < groups.length(); i++) {
				JSONObject jsonObj = groups.getJSONObject(i);

				int id = jsonObj.getInt("id");
				String title = jsonObj.getString("title");

				groupList.add(new Group(id, title));
			}
		}
		catch (Exception e) {
			Log.d("Erreur", "Il y a une erreur au chargement du contact");
		}
		return groupList;
	}

	public static ArrayList<Group> getGroupList() {
		return groupList;
	}
}

