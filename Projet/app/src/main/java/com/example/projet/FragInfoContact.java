package com.example.projet;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.projet.controller.GContact;
import com.example.projet.model.Contact;
import com.example.projet.model.Group;
import com.example.projet.model.MailAddress;
import com.example.projet.model.Phone;
import com.example.projet.model.PostalAddress;
import com.example.projet.request.ContactRequest;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragInfoContact.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragInfoContact#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragInfoContact extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ContactRequest reqContact = new ContactRequest();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragInfoContact() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragInfoContact.
     */
    // TODO: Rename and change types and number of parameters
    public static FragInfoContact newInstance(String param1, String param2) {
        FragInfoContact fragment = new FragInfoContact();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_frag_info_contact, container, false);
        int id = GContact.getIdTransmitted();
        loadInfos(v, id);
        Button btnBack = v.findViewById(R.id.backContact);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickBack();
            }
        });
        Button btnEdit = v.findViewById(R.id.editBtn);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadEditFrag();
            }
        });
        return v;
    }


    public void loadEditFrag(){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction=manager.beginTransaction();
        FragEditContact fragmentContacts = new FragEditContact();
        transaction.replace(R.id.layoutFrag, fragmentContacts);
        transaction.commit();
    }


    public void clickBack(){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction=manager.beginTransaction();
        FragContacts fragmentContacts = new FragContacts();
        transaction.replace(R.id.layoutFrag, fragmentContacts);
        transaction.commit();
    }

    // Recupere et affiche toutes les infos concernant le contact
    public void loadInfos(View v, int id){
        Contact contact = GContact.getContactList().get(id);
        reqContact.loadGroups(contact.getId());

        ((TextView) v.findViewById(R.id.name)).setText(contact.getFirstname() + " " + contact.getLastname());

        String postalAddresses = "Adresses : \n";
        for ( PostalAddress address : contact.getAddress()) {
            postalAddresses += address.getAddress() + "     (" + address.getType() + ")\n";
        }
        ((TextView) v.findViewById(R.id.addresses)).setText(postalAddresses);

        String mailAddresses = "Mails : \n";
        for ( MailAddress address : contact.getEmail()) {
            mailAddresses += address.getMail() + "     (" + address.getType() + ")\n";
        }
        ((TextView) v.findViewById(R.id.mails)).setText(mailAddresses);

        String phone = "Numéro de téléphone : \n";
        for ( Phone phoneNbr : contact.getPhone()) {
            phone += phoneNbr.getPhone() + "     (" + phoneNbr.getType() + ")\n";
        }
        ((TextView) v.findViewById(R.id.phones)).setText(phone);

        String groups = "Groupes dans lesquels ce contact apparaît : \n";
        for (Group group : GContact.getContactGroup()){
            groups += group.getTitle() + " \n";
        }
        ((TextView) v.findViewById(R.id.groups)).setText(groups);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
