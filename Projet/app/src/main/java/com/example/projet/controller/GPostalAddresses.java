package com.example.projet.controller;

import android.util.Log;

import com.example.projet.model.Contact;
import com.example.projet.model.PostalAddress;

import org.json.JSONArray;
import org.json.JSONObject;

public class GPostalAddresses {


    public static void loadAddresses(JSONArray addresses) {

        try {
            for (int i = 0; i < addresses.length(); i++) {
                PostalAddress address = new PostalAddress();
                JSONObject jsonObj = addresses.getJSONObject(i);
                address.setAddress(jsonObj.getString("postalAddress"));
                address.setType(jsonObj.getString("type"));
                Contact contact = GContact.getContactList().get(jsonObj.getInt("person_id"));
                contact.getAddress().add(address);
            }
        }
        catch (Exception e) {
            Log.d("Erreur", "Il y a une erreur");
        }
    }
}
